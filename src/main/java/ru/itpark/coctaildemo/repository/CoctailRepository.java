package ru.itpark.coctaildemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.coctaildemo.entity.Coctail;

import java.util.List;
@Repository
public interface CoctailRepository extends JpaRepository<Coctail, Integer> {
    List<Coctail>findAllByNameContainsIgnoreCase(String name);
    List<Coctail>findAllByIngredientsContainsIgnoreCase(String name);


}
