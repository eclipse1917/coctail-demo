package ru.itpark.coctaildemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.coctaildemo.entity.Alcohol;

import java.util.List;

@Repository
public interface AlcoholRepository extends JpaRepository<Alcohol,Integer>{
    List<Alcohol> findAllByNameContainsIgnoreCase(String name);


}
