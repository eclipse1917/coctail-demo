package ru.itpark.coctaildemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.coctaildemo.entity.Syrop;

@Repository
public interface SyropRepository extends JpaRepository<Syrop, Integer> {
}
