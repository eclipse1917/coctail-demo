package ru.itpark.coctaildemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.coctaildemo.entity.Basket;
@Repository

public interface BasketRepository extends JpaRepository<Basket,Integer> {
}
