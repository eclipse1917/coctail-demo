package ru.itpark.coctaildemo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Alcohol {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private int price;
    private int Vol;
    private int AlcVol;


    public Alcohol() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getVol() {
        return Vol;
    }

    public void setVol(int vol) {
        Vol = vol;
    }

    public int getAlcVol() {
        return AlcVol;
    }

    public void setAlcVol(int alcVol) {
        AlcVol = alcVol;
    }
}
