package ru.itpark.coctaildemo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Syrop {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private int price;
    private int Vol;
    private String description;

    public Syrop(String name, int price, int vol, String description) {
        this.name = name;
        this.price = price;
        Vol = vol;
        this.description = description;
    }

    public Syrop() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getVol() {
        return Vol;
    }

    public void setVol(int vol) {
        Vol = vol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
