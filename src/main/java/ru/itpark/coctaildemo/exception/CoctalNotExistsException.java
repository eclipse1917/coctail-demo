package ru.itpark.coctaildemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Coctail not exists")
public class CoctalNotExistsException extends RuntimeException {
}
