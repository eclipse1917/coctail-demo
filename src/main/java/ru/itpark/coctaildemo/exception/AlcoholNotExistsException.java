package ru.itpark.coctaildemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Alcohol not exists")
public class AlcoholNotExistsException extends RuntimeException {
}
