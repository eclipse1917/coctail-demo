package ru.itpark.coctaildemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoctailDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoctailDemoApplication.class, args);
	}
}
