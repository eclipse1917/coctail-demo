package ru.itpark.coctaildemo.service;

import ru.itpark.coctaildemo.entity.Basket;

import java.util.List;

public interface BasketService {
    int summ();


    List<Basket> basketList();

}
