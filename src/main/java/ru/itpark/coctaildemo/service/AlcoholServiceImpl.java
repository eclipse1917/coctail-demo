package ru.itpark.coctaildemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itpark.coctaildemo.entity.Alcohol;
import ru.itpark.coctaildemo.entity.Coctail;
import ru.itpark.coctaildemo.exception.AlcoholNotExistsException;
import ru.itpark.coctaildemo.repository.AlcoholRepository;
import ru.itpark.coctaildemo.repository.BasketRepository;
import ru.itpark.coctaildemo.repository.CoctailRepository;

import java.util.List;


@Service
public class AlcoholServiceImpl implements AlcoholService {
    private final AlcoholRepository alcoholRepository;
    private final CoctailRepository coctailRepository;
    private final BasketRepository basketRepository;


    @Autowired
    public AlcoholServiceImpl(AlcoholRepository alcoholRepository, CoctailRepository coctailRepository, BasketRepository basketRepository) {
        this.alcoholRepository = alcoholRepository;
        this.coctailRepository = coctailRepository;
        this.basketRepository = basketRepository;
    }


    @Override
    public List<Alcohol> alcoholList() {
        return alcoholRepository.findAll();
    }

    @Override
    public void addAlcohol(Alcohol alcohol) {
        alcoholRepository.save(alcohol);

    }

    @Override
    public List<Alcohol> findByName(String name) {
        return alcoholRepository.findAllByNameContainsIgnoreCase(name);
    }

    @Override
    public void delete(int id) {
        if (!alcoholRepository.exists(id)){
            throw new AlcoholNotExistsException();
        }
        alcoholRepository.delete(id);
    }

    @Override
    public Alcohol findById(int id) {
        if (!alcoholRepository.exists(id)){
            throw new AlcoholNotExistsException();
        }
        return alcoholRepository.findOne(id);
    }

    @Override
    public List<Coctail> coctaillist(int id) {
        if (!alcoholRepository.exists(id)){
            throw new AlcoholNotExistsException();
        }
          return coctailRepository.findAllByIngredientsContainsIgnoreCase(alcoholRepository.getOne(id).getName());

    }

    @Override
    public void addInBasket(int id) {

    }

}
