package ru.itpark.coctaildemo.service;

import ru.itpark.coctaildemo.entity.Alcohol;
import ru.itpark.coctaildemo.entity.Coctail;

import java.util.List;

public interface AlcoholService {
    List<Alcohol> alcoholList();

    void addAlcohol(Alcohol alcohol);

    List<Alcohol> findByName(String name);

    void delete(int id);

    Alcohol findById(int id);

    List<Coctail> coctaillist(int id);


    void addInBasket(int id);

}
