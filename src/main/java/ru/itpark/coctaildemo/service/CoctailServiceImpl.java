package ru.itpark.coctaildemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itpark.coctaildemo.entity.Coctail;
import ru.itpark.coctaildemo.exception.CoctalNotExistsException;
import ru.itpark.coctaildemo.repository.CoctailRepository;

import java.util.List;

@Service
public class CoctailServiceImpl implements CoctailService {
    private final CoctailRepository coctailRepository;

    @Autowired
    public CoctailServiceImpl(CoctailRepository coctailRepository) {
        this.coctailRepository = coctailRepository;
    }

    @Override
    public List<Coctail> findAll(){
        return coctailRepository.findAll();
    }
    @Override
    public void add(Coctail coctail){
        coctailRepository.save(coctail);
    }

    @Override
    public List<Coctail> findByName(String name) {
        return coctailRepository.findAllByNameContainsIgnoreCase(name);
    }

    @Override
    public void delete(int id) {
        if (!coctailRepository.exists(id))
        {
            throw new CoctalNotExistsException();
        }
        coctailRepository.delete(id);
    }

    @Override
    public void edit(int id, Coctail coctail) {
        if (coctailRepository.findOne(id).getId() == id) {
            coctailRepository.findOne(id).setName(coctail.getName());
            coctailRepository.findOne(id).setHistory(coctail.getHistory());
            coctailRepository.findOne(id).setIngredients(coctail.getIngredients());
            coctailRepository.findOne(id).setMethodOfPreparation(coctail.getMethodOfPreparation());

        }
        else throw new CoctalNotExistsException();

    }

    @Override
    public Coctail findById(int id) {
        if (!coctailRepository.exists(id))
        {
            throw new CoctalNotExistsException();
        }
        return coctailRepository.findOne(id);
    }


}
