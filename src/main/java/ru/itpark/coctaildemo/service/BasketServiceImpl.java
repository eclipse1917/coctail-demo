package ru.itpark.coctaildemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itpark.coctaildemo.entity.Basket;
import ru.itpark.coctaildemo.repository.AlcoholRepository;
import ru.itpark.coctaildemo.repository.BasketRepository;
import ru.itpark.coctaildemo.repository.SyropRepository;

import java.util.List;

@Service
public class BasketServiceImpl implements BasketService {
    private final AlcoholRepository alcoholRepository;
    private final SyropRepository syropRepository;
    private final BasketRepository basketRepository;
    @Autowired
    public BasketServiceImpl(AlcoholRepository alcoholRepository, SyropRepository syropRepository, BasketRepository basketRepository) {
        this.alcoholRepository = alcoholRepository;
        this.syropRepository = syropRepository;
        this.basketRepository = basketRepository;
    }


    @Override
    public int summ(){
        return alcoholRepository.findOne(1).getPrice();
    }

    @Override
    public List<Basket> basketList() {
        return null;
    }




}
