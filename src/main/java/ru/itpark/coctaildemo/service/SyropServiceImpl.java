package ru.itpark.coctaildemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itpark.coctaildemo.entity.Syrop;
import ru.itpark.coctaildemo.repository.SyropRepository;

import java.util.List;

@Service
public class SyropServiceImpl implements SyropService {
    private final SyropRepository syropRepository;

    @Autowired
    public SyropServiceImpl(SyropRepository syropRepository) {
        this.syropRepository = syropRepository;
    }

    @Override
    public List<Syrop> syropList(){
        return syropRepository.findAll();
    }
    @Override
    public void addSyrop(Syrop syrop){
        syropRepository.save(syrop);

    }
}
