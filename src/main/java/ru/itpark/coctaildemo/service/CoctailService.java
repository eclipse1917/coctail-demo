package ru.itpark.coctaildemo.service;

import ru.itpark.coctaildemo.entity.Coctail;

import java.util.List;

public interface CoctailService {
    List<Coctail> findAll();

    void add(Coctail coctail);

    List<Coctail> findByName(String name);

    void delete(int id);


    void edit(int id, Coctail coctail);

    Coctail findById(int id);
}
