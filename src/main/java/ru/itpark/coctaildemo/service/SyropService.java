package ru.itpark.coctaildemo.service;

import ru.itpark.coctaildemo.entity.Syrop;

import java.util.List;

public interface SyropService {
    List<Syrop> syropList();

    void addSyrop(Syrop syrop);
}
