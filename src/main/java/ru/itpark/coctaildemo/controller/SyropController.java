package ru.itpark.coctaildemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itpark.coctaildemo.entity.Syrop;
import ru.itpark.coctaildemo.service.SyropService;

import java.util.List;

@RestController
@RequestMapping("/syrop")
public class SyropController {
    private final SyropService syropService;

    @Autowired
    public SyropController(SyropService syropService) {
        this.syropService = syropService;
    }
    //получить список сиропов
    @GetMapping
    public List<Syrop> syropList(){
        return syropService.syropList();
    }
    //добавить сироп
    @PostMapping
    public void addSyrop(@RequestBody Syrop syrop){
        syropService.addSyrop(syrop);
    }

}
