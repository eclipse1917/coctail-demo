package ru.itpark.coctaildemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itpark.coctaildemo.entity.Coctail;
import ru.itpark.coctaildemo.service.CoctailService;

import java.util.List;

@RestController
@RequestMapping("/coctail")
public class CoctailController {
    private final CoctailService coctailService;

    @Autowired
    public CoctailController(CoctailService coctailService) {
        this.coctailService = coctailService;
    }

    //получить список коктелей
    @GetMapping
    public List<Coctail> getAll(){
        return coctailService.findAll();
    }
    //получить коктель по id
    @GetMapping("/{id}")
    public Coctail findById(@PathVariable int id){
        return coctailService.findById(id);
    }
    //найти коктейль по имени
    @GetMapping(path = "findByName", params = "name")
    public List<Coctail> findByName(@RequestParam String name) {
        return coctailService.findByName(name);

    }
    //добавить коктейль
    @PostMapping
    public void add(@RequestBody Coctail coctail){
        coctailService.add(coctail);


    }
    //удалить коктейль
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        coctailService.delete(id);
    }
    //попытка отредактировать коктейль.
    @PutMapping("/{id}")
    public void edit (@PathVariable int id, @RequestBody Coctail coctail){
        coctailService.edit(id,coctail);
    }

}

