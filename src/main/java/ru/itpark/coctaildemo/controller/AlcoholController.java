package ru.itpark.coctaildemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itpark.coctaildemo.entity.Alcohol;
import ru.itpark.coctaildemo.entity.Coctail;
import ru.itpark.coctaildemo.service.AlcoholService;

import java.util.List;

@RestController
@RequestMapping("/alcohol")
public class AlcoholController {
    private final AlcoholService alcoholService;

    @Autowired
    public AlcoholController(AlcoholService alcoholService) {
        this.alcoholService = alcoholService;
    }

    //получить список алкоголя
    @GetMapping
    public  List<Alcohol> alcoholList(){
        return alcoholService.alcoholList();
    }
    // найти алкоголь по имени
    @GetMapping(path = "findByName", params = "name")
    public List<Alcohol> findByName(@RequestParam String name) {
        return alcoholService.findByName(name);
    }
    // получить алкоголь по id
    @GetMapping("/{id}")
    public Alcohol findById(@PathVariable int id){
        return alcoholService.findById(id);
    }
    //найти коктейль по позванию алкоголя
    @GetMapping(path = "findCoctail",params = "id")
    public List<Coctail> coctailList(@RequestParam int id){
        return alcoholService.coctaillist(id);
    }
    //добавить алкоголь
    @PostMapping
    public void addAlcohol(@RequestBody Alcohol alcohol){
        alcoholService.addAlcohol(alcohol);
    }
    //удалить алкоголь
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        alcoholService.delete(id);
    }

    // не могу понять как реализовать добавление алкоголя по id в корзину
    @GetMapping(path = "addAlcoholInBasket",params = "id")
    public void addAlcoholInBasket(@RequestParam int id){
       alcoholService.addInBasket(id);
    }


}
